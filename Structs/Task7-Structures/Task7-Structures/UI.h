#pragma once
#include "Staff.h"

void InputWorkerInfo(TWorker& worker);

void InputStaff(TWorker* staff);

int FindWorkerByCode(TWorker* staff, int key);

void Interface();
