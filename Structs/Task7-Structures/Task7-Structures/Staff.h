#pragma once
#include "Worker.h"

#define WORKERS_QUANTITY 3

double FindWorkerWithMaxSalary(TWorker* staff, char* name);
bool DeleteWorker(TWorker* staff, int key);
