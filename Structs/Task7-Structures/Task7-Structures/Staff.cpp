#include "stdafx.h"
#include "Staff.h"

double FindWorkerWithMaxSalary(TWorker* staff, char* name)
{
	double max_salary = 0.0;

	max_salary = staff[0].salary;
	strcpy_s(name, 30, staff[0].name);

	for (int i = 1; i < WORKERS_QUANTITY; i++)
	{
		if (staff[i].salary > max_salary)
		{
			max_salary = staff[i].salary;
			strcpy_s(name, 30, staff[i].name);
		}
	}

	return max_salary;
}

int FindWorkerByCode(TWorker* staff, int key)
{
	int index = 0;
	while (index < WORKERS_QUANTITY && staff[index].code != key)
		index++;

	return index;
}

bool DeleteWorker(TWorker* staff, int key)
{
	int index = FindWorkerByCode(staff, key);

	if (index < WORKERS_QUANTITY)
	{
		staff[index].is_free = true;
		staff[index].code = 0;
		
	}

	return index < WORKERS_QUANTITY;
}