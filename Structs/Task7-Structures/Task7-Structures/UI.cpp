#include "stdafx.h"
#include "UI.h"
#include "Staff.h"

void InputWorkerInfo(TWorker& worker, TWorker staff[])
{
	cout << "Enter worker name" << "\n";
	cin >> worker.name;

	while (true)
	{
		cout << "Enter worker code" << "\n";
		int code = 0;
		cin >> code;
		int index = FindWorkerByCode(staff, code);
		if (index == WORKERS_QUANTITY) 
		{
			worker.code = code;
			break;
		}
		cout << "ERROR: code match" << "\n";
	}
	

	cout << "Enter worker salary" << "\n";
	cin >> worker.salary;

	worker.is_free = false;

	system("pause");
	system("cls");
}

void InputStaff(TWorker* staff)
{
	int i = 0;
	for (; i < WORKERS_QUANTITY && !staff[i].is_free; i++);

	if (i < WORKERS_QUANTITY )
		InputWorkerInfo(staff[i], staff);
	else
		cout << "ERROR: array is full..." << "\n\n";
}

void ShowAllWorkers(TWorker* staff)
{
	cout << "Code\tName\tSalary" << endl;
	for (int index = 0; index < WORKERS_QUANTITY; index++)
	{
		if(!staff[index].is_free)
			cout << staff[index].code << "\t" << staff[index].name << "\t" << staff[index].salary << "\n\n";
	}

	system("pause");
	system("cls");
}

void Interface()
{
	unsigned short int action = 0;

	char name[30] = "\0";
	double max_salary = 0.0;

	TWorker staff[WORKERS_QUANTITY];

	do {
		cout << "1-Add worker" << "\n\n";
		cout << "2-Get worker with max salary" << "\n\n";
		cout << "3-Show all workers" << "\n\n";
		cout << "4-Delete worker by key" << "\n\n";
		cout << "5-Exit" << "\n\n";
		
		cin >> action;

		switch (action)
		{
			case 1: {
				system("cls");
				InputStaff(staff); 
				break;
			}
			case 2: {
				system("cls");
				max_salary = FindWorkerWithMaxSalary(staff, name);
				
				cout << "Max salary: " << max_salary << endl;
				cout << "Worker name: " << name << endl;
				system("pause");
				system("cls");
				break;
			}
			case 3: {
				system("cls");
				ShowAllWorkers(staff);
				break; 
			}
			case 4: {
				system("cls");

				int key = 0;
				cout << "Enter key value: ";
				cin >> key;

				if ( DeleteWorker(staff, key) )
					cout << "Worker with code " << key << " deleted..." << endl;
				else
					cout << "Worker not found..." << endl;
				break;
			}
			case 5: break;

			default: {
				cout << "ERROR: Wrong action..." << endl; 
				system("pause");
				system("cls");
				break;
			}
		}
		
	} while (action != 5);
}
