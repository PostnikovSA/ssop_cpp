using namespace std;

void EnterWorkerInfo(TWorker& worker)
{
	if (countStaff >= sizeStaff) 
	{
		cout << "Error. Max limit staff: " << sizeStaff << "\n\n";
	}
	else 
	{
		bool isCorrect = false;
		cout << "Enter name: " << "\n";
		cin >> worker.name;
		cout << "\n";

		isCorrect = false;
		while (isCorrect == false)
		{
			int code = 0;

			cout << "Enter code from 1(Full-Time) to 2(Part-Time): " << "\n";
			cin >> code;
			switch (code)
			{
			case 1:
				worker.code = code;
				isCorrect = true;
				break;
			case 2:
				worker.code = code;
				isCorrect = true;
				break;

			default:
				isCorrect = false;
				cout << "Error" << "\n\n";
				break;
			}
		}
		cout << "\n";

		isCorrect = false;
		cout << "Enter salary: " << "\n";
		cin >> worker.salary;
		cout << "\n";

		countStaff++;
	}
}

void PrintWorkerInfo(TWorker worker)
{
	cout << "Name: " << worker.name << "\n";
	cout << "Code: " << worker.code << "\n";
	cout << "Salary: " << worker.salary << "\n";
	
	cout << "\n";
}

void PrintWorkersInfo(TWorker staff[], int countStaff)
{
	if (countStaff <= 0) 
		cout << "No workers data" << "\n\n";
	else
	{
		for (int i = 0; i < countStaff; i++)
		{
			cout << "Worker" << i + 1 << "\n";
			cout << "Name: " << staff[i].name << "\n";
			cout << "Code: " << staff[i].code << "\n";
			cout << "Salary: " << staff[i].salary << "\n";
			cout << "\n";
		}
		cout << "\n";
	}
}

void PrintWorkerInfoWithMaxSalary() 
{
	if (countStaff <= 0)
	{
		cout << "No workers data" << "\n\n";
	}
	else 
	{
		cout << staff[GetIndexWorkerWithMaxSalary(staff, countStaff)].name << "\n";
		cout << staff[GetIndexWorkerWithMaxSalary(staff, countStaff)].code << "\n";
		cout << staff[GetIndexWorkerWithMaxSalary(staff, countStaff)].salary << "\n";
	}
}

void PrintCountWorkersFullTime() 
{
	cout << "Count workers Full-Time: " << GetCountWorkersFullTime(staff, countStaff) << "\n";
}

void RunMenu()
{
	bool isExit = false;
	
	while (isExit == false)
	{
		cout << "Menu" << "\n\n";
		cout << "[1] Add worker" << "\n";
		cout << "[2] Print info worker with max salary" << "\n";
		cout << "[3] Print count workers full-time" << "\n";
		cout << "[4] Print info all workers" << "\n";
		cout << "[5] Exit" << "\n\n";

		cout << "Enter only number from 1 to 5: ";
		int numMenu = 0;
		cin >> numMenu;
		cout << "\n";

		switch (numMenu)
		{
		case 1:
			system("cls");
			cout << "Run Add worker" << "\n\n";
			EnterWorkerInfo(staff[countStaff]);
			system("pause");
			system("cls");
			break;
		case 2:
			system("cls");
			cout << "Run Print worker info with max salary" << "\n\n";
			PrintWorkerInfoWithMaxSalary();
			system("pause");
			system("cls");
			break;
		case 3:
			system("cls");
			cout << "Run Print count workers Full-Time" << "\n\n";
			PrintCountWorkersFullTime();
			system("pause");
			system("cls");
			break;
		case 4:
			system("cls");
			cout << "Run Print info all workers" << "\n\n";
			PrintWorkersInfo(staff, countStaff);
			system("pause");
			system("cls");
			break;
		case 5:
			system("cls");
			cout << "Run Exit" << "\n\n";
			isExit = true;
			system("pause");
			system("cls");
			break;
			
		default:
			system("cls");
			cout << "Error. Try again. Enter only number from 1 to 5" << "\n\n";
			system("pause");
			system("cls");
			break;
		}
	}
}