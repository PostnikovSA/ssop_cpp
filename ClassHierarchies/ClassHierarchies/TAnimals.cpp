
#include "TAnimals.h"
#include <iostream>
using namespace std;

void TAnimals::parameters(double height, double weight)
{
	cout << "Species: " << Species << endl;
	cout << "Name: " << Name << endl;
	cout << "Gender: " << Gender << endl;
	cout << "Color: " << Color << endl;
	cout << "Age: " << Age << endl;
	cout << "Height: " << height << " cm" << endl;
	cout << "Weight: " << weight << " kg" << endl;
}