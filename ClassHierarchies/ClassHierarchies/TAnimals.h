#pragma once

class TAnimals
{
private:
	char* Species;
	char* Name;
	char* Gender;
	char* Color;
	int Age;
public:
	TAnimals(char* species, char* name, char* gender, char* color, int age)
	{
		Species = species;
		Name = name;
		Gender = gender;
		Color = color;
		Age = age;
	};
	virtual void parameters(double height, double weight);
};
