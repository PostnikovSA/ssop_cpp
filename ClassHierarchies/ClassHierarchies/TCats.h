
#pragma once

#include "TAnimals.h"
using namespace std;

class TCatsAnimals :public TAnimals
{
	char* CatBreed;

public:
	TCatsAnimals(char* species, char* name, char* gender, char* color, int age, char* cat_breed) :TAnimals(species, name, gender, color, age)
	{
		CatBreed = cat_breed;
	}
	virtual void parameters(double height, double weight);
};