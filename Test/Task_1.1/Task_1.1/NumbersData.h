
#pragma once

int GetMinNum();

int GetMaxNum();

int GetStep();

int GetMultiple();

int GetSumMultipleWithOperatorFor();

int GetSumMultipleWithOperatorWhile();

int GetSumMultipleWithOperatorDoWhile();