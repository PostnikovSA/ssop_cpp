
int minNum = 1;
int maxNum = 50;
int step = 1;
int multiple = 3;

int GetMinNum() 
{
	return minNum;
}

int GetMaxNum() 
{
	return maxNum;
}

int GetStep() 
{
	return step;
}

int GetMultiple()
{
	return multiple;
}

int GetSumMultipleWithOperatorFor()
{
    int sum = 0;

	for (int count = minNum; count < maxNum; count += step)
	{
		if ((count % multiple) == 0)
			sum += count;
	}

    return sum;
}

int GetSumMultipleWithOperatorWhile() 
{
	int sum = 0;
	int count = minNum;

	while (count < maxNum)
	{
		if (count % multiple == 0)
		{
			sum += count;
		}
		count++;
	}

	return sum;
}

int GetSumMultipleWithOperatorDoWhile() 
{
	int sum = 0;
	int count = minNum;

	do
	{
		if (count % multiple == 0)
		{
			sum += count;
		}
		count++;
	} 
	while (count < maxNum);

	return sum;
}