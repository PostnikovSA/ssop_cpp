
#include <iostream>
#include "TData.h"
using namespace std;

void Input(int** numArray, int n, int m)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            int num = 1000; // crutch
            while (true)
            {
                cout << "M[" << i << "][" << j << "]= ";

                while (!(cin >> num) || (cin.peek() != '\n'))
                {
                    cin.clear();
                    while (cin.get() != '\n');
                    break;
                }

                if ((num > 0) && (num < 100))
                    break;
            }
            numArray[i][j] = num;
        }
    }
}

void Change(int** M, int n, int m)
{
    for (int j = 0; j < m; j++)
    {
        int temp = 0;
        for (int i = 0; i < n; i++)
        {
            temp += M[i][j];
        }
        cout << "Average for column M[" << j << "]: " << ((double)temp / m) << "\n";
    }
    cout << "\n";
}

void Del(int** numArray, int n, int m)
{
    for (int i = 0; i < n; i++) 
        delete[] numArray[i];

    delete[]numArray;
    cout << "Array deleated\n";
}

int getN()
{
    int n = 0;
    while (true)
    {
        cout << "Input number Columns. Input only number from 2 to 10: ";

        while (!(cin >> n) || (cin.peek() != '\n'))
        {
            cin.clear();
            while (cin.get() != '\n');
            break;
        }

        if ((n <= 10) && (n > 1))
            break;
    }
    system("cls");
    return n;
}

int getM()
{
    int m = 0;
    while (true)
    {
        cout << "Input number Rows. Input only number from 2 to 10: ";

        while (!(cin >> m) || (cin.peek() != '\n'))
        {
            cin.clear();
            while (cin.get() != '\n');
            break;
        }

        if ((m <= 10) && (m > 1))
            break;
    }
    system("cls");
    return m;
}
