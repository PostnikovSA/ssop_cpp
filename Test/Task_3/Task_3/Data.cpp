#include <iostream>
#include "Data.h"
using namespace std;

int getN()
{
	int n = 0;
	while (true)
	{
		cout << "Input only number from 1 to 10: ";

		while (!(cin >> n) || (cin.peek() != '\n'))
		{
			cin.clear();
			while (cin.get() != '\n');
			break;
		}

		if ((n <= 10) && (n > 0))
			break;
	}
	system("cls");
	return n;
}

tNode* create_list(int N)
{
	tNode* p_begin = NULL;
	tNode* p = NULL;

	p_begin = (tNode*)malloc(sizeof(tNode));
	p = p_begin;
	p->next = NULL;
	p->value = 0;

	for (int k = 1; k < N; k++) 
	{
		p->next = (tNode*)malloc(sizeof(tNode));

		p = p->next;

		p->next = NULL;
		p->value = k;
	}
	return p_begin;
}

void print_list(tNode* p_begin)
{
	tNode* p = p_begin;
	while (p != NULL) 
	{
		cout << p->value << " ";
		p = p->next;
	}
	cout << "\n\n";
}

void delete_list(tNode* p_begin)
{
	tNode* p = p_begin;
	while (p != NULL)
	{
		tNode* tmp;
		tmp = p;

		p = p->next;

		free(tmp);
	}
	cout << "List deleted" << "\n\n";
}

void print_sum_arithmetic_mean_of_couples(tNode* p_begin)
{
	tNode* p = p_begin;
	int sum = 0;
	int elementCount = 0;
	double meanSum = 0;
	while (p != NULL) 
	{
		tNode* tmp;

		elementCount++;

		sum += p->value;

		if (elementCount % 2 == 0)
		{
			meanSum += sum / 2.0;
			sum = 0;
		}

		tmp = p;

		p = p->next;
	}

	if (elementCount % 2 == 1)
	{
		meanSum += sum;
	}
	cout << "Sum of arithmetic means is: " << meanSum << "\n\n";
}