#pragma once
#include <iostream>
#include "Data.h"
using namespace std;

void StartCreateList()
{
	int n = getN();

	tNode* p_begin = create_list(n);

	cout << "\nList created\n\n";
}


void Interface()
{
	system("cls");

	bool isExit = false;

	int n = getN();

	tNode* p_begin = create_list(n);

	bool isListExist = true;

	while (isExit == false)
	{
		cout << "Menu" << "\n\n";
		cout << "[1] Create List" << "\n";
		cout << "[2] Print List" << "\n";
		cout << "[3] Print sum arithmetic mean of couples" << "\n";
		cout << "[4] Delete List" << "\n";
		cout << "[5] Exit" << "\n\n";

		int numMenu = 0;
		while (!(cin >> numMenu) || (cin.peek() != '\n'))
		{
			cout << "Input only number from 1 to 6: ";
			cin.clear();
			while (cin.get() != '\n');
			break;
		}

		switch (numMenu)
		{
		case 1:
			system("cls");
			cout << "Run CreateList" << "\n\n";
			if (isListExist == true)
				cout << "List already exists\n\n";
			else
			{
				StartCreateList();
				isListExist = true;
			}
			system("pause");
			system("cls");
			break;

		case 2:
			system("cls");
			cout << "Run PrintList" << "\n\n";
			if (isListExist == false)
				cout << "List no exists\n\n";
			else
			{
				print_list(p_begin);
			}
			system("pause");
			system("cls");
			break;

		case 3:
			system("cls");
			cout << "Run Print sum arithmetic mean of couples" << "\n\n";
			if (isListExist == false)
				cout << "List no exists\n\n";
			else
			{
				print_sum_arithmetic_mean_of_couples(p_begin);
			}
			system("pause");
			system("cls");
			break;

		case 4:
			system("cls");
			cout << "Run DeleteList" << "\n\n";
			if (isListExist == true)
			{
				delete_list(p_begin);
				isListExist = false;
			}
			else
				cout << "List no exists\n\n";
			system("pause");
			system("cls");
			break;

		case 5:
			system("cls");
			cout << "Run Exit" << "\n\n";
			isExit = true;
			system("pause");
			system("cls");
			break;

		default:
			system("cls");
			cout << "Error. Try again. Input only number from 1 to 6" << "\n\n";
			system("pause");
			system("cls");
			break;
		}
	}
}

