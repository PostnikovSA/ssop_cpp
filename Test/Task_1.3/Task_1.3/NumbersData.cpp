
#include <iostream>
using namespace std;

double minNum = -4.0;
double maxNum = 4.0;
double step = 0.2;

double GetMinNum()
{
	return minNum;
}

double GetMaxNum()
{
	return maxNum;
}

double GetStep()
{
	return step;
}

double GetValue(double num)
{
	double value = 0.0;

	value = sin(num) / (2.0 + num);
		
	return value;
}

